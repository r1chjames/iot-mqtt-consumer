name := "iot-mqtt-consumer"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-api" % "1.7.5",

  "org.eclipse.paho" % "org.eclipse.paho.client.mqttv3" % "1.2.0",
  "com.typesafe.akka" %% "akka-actor" % "2.5.13",
  "com.paulgoldbaum" %% "scala-influxdb-client" % "0.6.0",


  "org.scalatest" %% "scalatest" % "3.2.0-SNAP10" % Test,
  "com.typesafe.akka" %% "akka-testkit" % "2.5.13" % Test
)
