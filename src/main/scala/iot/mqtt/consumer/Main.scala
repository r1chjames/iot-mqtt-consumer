package iot.mqtt.consumer

import iot.mqtt.consumer.consumers.MqttConsumer

object Main {

  def main(args:Array[String]) = {
    val mqttConsumer = new MqttConsumer
    mqttConsumer.run()
  }

}
