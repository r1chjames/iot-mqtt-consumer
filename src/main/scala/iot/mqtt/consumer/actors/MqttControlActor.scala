package iot.mqtt.consumer.actors

import akka.actor.Actor
import akka.event.Logging
import iot.mqtt.consumer.entities.MqttMsg

class MqttControlActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case MqttMsg(topic, message) => {
      if (isControlMessage(topic)) {
        log.info("Receiving Control Message, Topic : %s, Message : %s".format(topic, message))
      }
    }
  }

  def isControlMessage(topic: String): Boolean = {
    if (topic.contains("owntracks")) true
    else false
  }
}
