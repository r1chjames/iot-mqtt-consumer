package iot.mqtt.consumer.configuration

class Configuration(var host: String,
                         var port: Integer,
                         var topic: String,
                         var username: String,
                         var password: String) {
}
