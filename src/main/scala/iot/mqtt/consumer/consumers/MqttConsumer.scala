package iot.mqtt.consumer.consumers

import akka.actor.{ActorSystem, Props}
import iot.mqtt.consumer.actors.{MqttControlActor, MqttDataActor}
import iot.mqtt.consumer.configuration.Configuration
import iot.mqtt.consumer.entities.MqttMsg
import org.eclipse.paho.client.mqttv3._
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

class MqttConsumer() extends Runnable {

  val thread: Thread = new Thread(this)
  this.thread.start()


  def startSubscriber(): Unit = {
//    val configuration = new Configuration("192.168.0.58", 1883, "#", "homeassistant", "2032")
        val configuration = new Configuration("m21.cloudmqtt.com", 13888, "#", "nbzjiesj", "-eIoQuiYPOpb")
    val brokerUrl: String = "tcp://%s:%s".format(configuration.host, configuration.port)
    val topic: String = configuration.topic

    val persistence = new MemoryPersistence
    val opts = new MqttConnectOptions()
    opts.setUserName(configuration.username)
    opts.setPassword(configuration.password.toCharArray)

    val client = new MqttClient(brokerUrl, "iot-mqtt-connector", persistence)
    client.connect(opts)
    client.subscribe(topic)

    client.setCallback(callback)
  }


  override def run(): Unit = {
    startSubscriber()
  }

  def destroy(): Unit = thread.interrupt()

  val callback: MqttCallback = new MqttCallback {

    override def messageArrived(topic: String, message: MqttMessage): Unit = {
      val system = ActorSystem("MQTTSystem")

      val mqttMessage = MqttMsg(topic, message)
      val mqttDataActor = system.actorOf(Props[MqttDataActor], name = "MqttDataActor")
      val mqttControlActor = system.actorOf(Props[MqttControlActor], name = "MqttControlActor")
      mqttDataActor ! mqttMessage
      mqttControlActor ! mqttMessage
    }

    override def connectionLost(cause: Throwable): Unit = {
      println(cause)
    }

    override def deliveryComplete(token: IMqttDeliveryToken): Unit = {}
  }


}
