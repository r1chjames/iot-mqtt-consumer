package iot.mqtt.consumer.dao

import com.paulgoldbaum.influxdbclient.Point
import iot.mqtt.consumer.entities.NoSqlRecord

trait NoSqlDbInterface {

  def connect(connectionString: String): Unit

  def save(record: NoSqlRecord): Unit

  def save(point: Point): Unit

  def close(): Unit

}
