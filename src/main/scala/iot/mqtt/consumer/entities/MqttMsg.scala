package iot.mqtt.consumer.entities

import org.eclipse.paho.client.mqttv3.MqttMessage

case class MqttMsg(topic: String, message: MqttMessage) {

}
